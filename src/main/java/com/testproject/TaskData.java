package com.testproject;

public class TaskData implements java.io.Serializable{

    private static final long serialVersionUID = -7415334054316916637L;

    private String pathCd;

    public String getPathCd() {
        return pathCd;
    }

    public void setPathCd(String pathCd) {
        this.pathCd = pathCd;
    }

}
