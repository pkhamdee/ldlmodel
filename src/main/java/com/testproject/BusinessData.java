package com.testproject;

public class BusinessData implements java.io.Serializable{


    private static final long serialVersionUID = 7328433651625910756L;

    private String businessChannel;

    public String getBusinessChannel() {
        return businessChannel;
    }

    public void setBusinessChannel(String businessChannel) {
        this.businessChannel = businessChannel;
    }

}
