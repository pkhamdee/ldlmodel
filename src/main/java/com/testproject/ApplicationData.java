package com.testproject;

public class ApplicationData implements java.io.Serializable{

    private static final long serialVersionUID = 4823475707798598163L;

    private String projectType;
    private BusinessData businessData;
    private TaskData taskData;

    public BusinessData getBusinessData() {
        return businessData;
    }

    public void setBusinessData(BusinessData businessData) {
        this.businessData = businessData;
    }

    public String getProjectType() {
        return projectType;
    }

    public void setProjectType(String projectType) {
        this.projectType = projectType;
    }

    public TaskData getTaskData() {
        return taskData;
    }

    public void setTaskData(TaskData taskData) {
        this.taskData = taskData;
    }
}
